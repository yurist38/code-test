import {
    capitalizeEachWord,
    capitalizeFirstLetter,
    spacesToUnderscores,
    toSnakeCase,
} from '../strings'

describe('String utils', () => {
    describe('function capitalizeFirstLetter', () => {
        it('should capitalize the first letter', () => {
            const converted = capitalizeFirstLetter('hello world')
            const expected = 'Hello world'
            expect(converted).toEqual(expected)
        })
    })
    describe('function capitalizeEachWord', () => {
        it('should capitalize the first letter of each word', () => {
            const converted = capitalizeEachWord('hello world')
            const expected = 'Hello World'
            expect(converted).toEqual(expected)
        })
    })

    describe('function toSnakecase', () => {
        it('converts camelCase to snake_case', () => {
            expect(toSnakeCase('camelCase')).toEqual('camel_case')
        })
        it('converts PascalCase to snake_case', () => {
            expect(toSnakeCase('PascalCase')).toEqual('pascal_case')
        })
        it('converts ALLCASE to lowercase', () => {
            expect(toSnakeCase('ALLCASE')).toEqual('allcase')
        })
        it('leave lowercase alone', () => {
            expect(toSnakeCase('lowercase')).toEqual('lowercase')
        })
        it('leave snake_case alone', () => {
            expect(toSnakeCase('snake_case')).toEqual('snake_case')
        })
    })

    describe('function spaceToUnderscore', () => {
        it('doesnt change strings without spaces', () => {
            expect(spacesToUnderscores('_aA2_bB2__cC3___')).toEqual('_aA2_bB2__cC3___')
        })
        it('converts spaces to underscores', () => {
            expect(spacesToUnderscores('Aa1 Bb2')).toEqual('Aa1_Bb2')
        })
        it('converts spaces to underscores on edges', () => {
            expect(spacesToUnderscores(' Aa1Bb2 ')).toEqual('_Aa1Bb2_')
        })
        it('converts multiple spaces to multiple underscores', () => {
            expect(spacesToUnderscores('A a1   Bb 2')).toEqual('A_a1___Bb_2')
        })
    })
})
