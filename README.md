## Digib CodeTest
This repository contains a selection of the tests used to cover some of the utility functions used in [Brenntag Source][1].
The utility functions are omited. The goal of the exercise is to make the tests pass. 

The tech stack used is:
- [ES6][2]
- [Typescript][3]
- [Jest][4]

#### Clone repository
```$ git clone https://digib-ruben@bitbucket.org/digib-ruben/code-test.git```

##### Setup 
```$ npm install```

##### Running all tests
```$ npm test```

##### Running a specific file with tests
```$ npm test <path-to-file-with-tests>```

#### Creating and pushing your branch
Solutions to the exercise should be submitted as a separate branch

```    
$ git checkout -b [BRANCH_NAME]
$ git commit -am '[COMMIT MESSAGE]'
$ git push --set-upstream origin
```

[1]: http://www.brenntagsource.com
[2]: https://en.wikipedia.org/wiki/ECMAScript#6th_Edition_-_ECMAScript_2015
[3]: http://www.typescriptlang.org
[4]: https://jestjs.io